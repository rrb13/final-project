import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";
import {Helmet} from "react-helmet"

function Table() {
  const { products, fetchProducts } = useContext(GlobalContext);

  const onDelete = async (id) => {
    try {
      // mengirimkan sebuah delete request
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      fetchProducts();
      alert("Berhasil menghapus product");
    } catch (error) {
      alert("Terjadi Sesuatu Error");
    }
  };
  return (
    
    <section className="max-w-7xl mx-auto my-10">
      <Helmet>
            <title>Table Page</title>
      </Helmet>
      <h1 className="my-8 text-3xl font-bold text-center">Table Product</h1>
      <div class="flex justify-end">
          <Link to="/create" 
           className="text-orange-400
              bg-white
                border-2 border-orange-400
                font-medium rounded-lg
                text-sm px-4 py-2
                text-center"
            >
              Create Product
          </Link>
      </div>
      <div className="mx-auto w-full my-4">
        <table className="w-full text-sm text-left -text-gray-500">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50">
            <tr>
              <th scope="col" className="px-6 py-3">ID</th>
              <th scope="col" className="px-6 py-3">Nama</th>
              <th scope="col" className="px-6 py-3">Status Diskon</th>
              <th scope="col" className="px-6 py-3">Harga</th>
              <th scope="col" className="px-6 py-3">Harga Diskon</th>
              <th scope="col" className="px-6 py-3">Gambar</th>
              <th scope="col" className="px-6 py-3">Kategori</th>
              <th scope="col" className="px-6 py-3">Action</th>
            </tr>
          </thead>
          <tbody>
            
            {products.map((product, index) => {
              return (
                <tr key={product.id} className="bg-white border-b hover:bg-gray-50 px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                  <td className="px-6 py-4">{product.id}</td>
                  <td className="px-6 py-4">{product.name}</td>
                  <td className="px-6 py-4">
                    {product.is_diskon === true ? "diskon" : "tidak diskon"}
                  </td>
                  <td className="px-6 py-4">{product.harga_display}</td>
                  <td className="px-6 py-4">{product.harga_diskon_display}</td>
                  <td className="px-6 py-4">
                    <img src={product.image_url} alt="" className="w-64" />
                  </td>
                  <td className="px-6 py-4">{product.category}</td>
                  <td className="px-6 py-4">
                    <div className="flex gap-2">
                      <Link to={`/update/${product.id}`}
                        className="text-white 
                            bg-orange-400 
                            hover:bg-orange-500 
                            font-medium rounded-lg 
                            text-sm px-4 py-2 
                            text-center mr-3 md:mr-0"
                        >
                          Update
                      </Link>
                      <button
                        onClick={() => {
                          onDelete(product.id);
                        }}
                        className="text-orange-400
                          bg-white
                            border-2 border-orange-400
                            font-medium rounded-lg
                            text-sm px-4 py-2
                            text-center"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default Table;
