import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <nav class="bg-white sticky w-full border-b-2 border-gray-200 shadow-xl">
        <div class="max-w-screen-full flex flex-wrap items-center justify-between m-5">
            <a href="/src/index.html" class="flex items-center">
                <img src="/logo.png" class="h-8 mr-3" alt="Logo Store"/>
                <span class="self-center text-2xl font-semibold">Man Cave</span>
            </a>
            <div class="flex md:order-2">
                <div>
                    <button 
                        type="button" 
                        class="text-orange-400 
                            bg-white
                            border-2 border-orange-400
                            hover:bg-orange-100 
                            font-medium rounded-lg 
                            text-sm px-4 py-2 
                            text-center mr-3"
                        >
                        Login
                    </button>
                    <button 
                        type="button" 
                        class="text-white 
                            bg-orange-400 
                            hover:bg-orange-500 
                            font-medium rounded-lg 
                            text-sm px-4 py-2 
                            text-center mr-3"
                        >
                        Register
                    </button>
                </div>
                <button data-collapse-toggle="navbar-sticky" 
                    type="button" 
                    class="inline-flex items-center p-2 w-10 h-10 
                        justify-center 
                        text-sm text-gray-500 
                        rounded-lg 
                        md:hidden 
                        hover:bg-gray-100" 
                    aria-controls="navbar-sticky" 
                    aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
                <ul class="flex flex-col p-4 
                    md:p-0 mt-4 
                    font-medium  

                    md:flex-row 
                    md:space-x-8 
                    md:mt-0 md:border-0 
                    md:bg-white">
                    <Link to="/" 
                      className="block py-2 pl-3 pr-4 
                                text-orange-400
                                rounded 
                                md:bg-transparent
                                md:hover:text-blue-700  
                                md:p-0" >
                      <li>
                        Home
                      </li>
                    </Link>
                    <Link to="/table" 
                      className="block py-2 pl-3 pr-4 
                                text-orange-400
                                rounded 
                                md:hover:text-blue-700  
                                md:p-0" >
                      <li>
                        Table
                      </li>
                    </Link>
                </ul>
            </div>
        </div>
    </nav>
  );
}

export default Navbar;
