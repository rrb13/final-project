import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";
import {useFormik} from "formik";
import Form from "./Form";
import * as Yup from "yup";
import {Helmet} from "react-helmet"

const validationSchema = Yup.object({
    name:Yup.string().required("Nama Barang Wajib Diisi"), //nama barang
    stock:Yup.number().required("Stock Barang Wajib Diisi"), //stock barang
    harga:Yup.number().required("Harga Barang Wajib Diisi"), //harga barang
    is_diskon:Yup.boolean(), //status diskon
    harga_diskon:Yup.number()
        .when('is_diskon',{
            is:true,
            then:() => Yup.number().required("Harga Diskon Wajib Diisi"),
        }),
    category:Yup.string().required("Kategori Barang Wajib Diisi"), //kategori barang
    image_url:Yup.string()
        .required("URL Gambar Wajib Diisi")
        .url("Format URL tidak valid"), //url gambar
    description:"", //deskripsi
})


function UpdateForm() {
    

  const { productId } = useParams();
  const navigate = useNavigate()
  const [input,setInput] = useState({
        name:"", //nama barang
        stock:0, //stock barang
        harga:0, //harga barang
        is_diskon:false, //status diskon
        harga_diskon:0,
        category:"", //kategori barang
        image_url:"", //url gambar
        description:"", //deskripsi
    })


  //handle submit yg diinput di form
    const onSubmit = async () => {
        try {
            //bikin data product baru
            console.log(productId)
            const response = await axios.put(
                `https://api-project.amandemy.co.id/api/products/${productId}`,
                {
                    name: values.name, //nama barang
                    stock: values.stock, //stock barang
                    harga: values.harga, //harga barang
                    harga_display:`Rp. ${values.harga}`,
                    is_diskon: values.is_diskon, //status diskon
                    harga_diskon: values.harga_diskon,
                    harga_diskon_display:`Rp. ${values.harga_diskon}`,
                    category: values.category, //kategori barang
                    image_url: values.image_url, //url gambar
                    description: values.description, //deskripsi
                }
            );
            // console.log(response)
            // bikin placeholder atau text area pada form kembali ke default(kosong)
            // setInput({
            //     name:"", //nama barang
            //     stock:0, //stock barang
            //     harga:0, //harga barang
            //     is_diskon:false, //status diskon
            //     harga_diskon:0,
            //     category:"", //kategori barang
            //     image_url:"", //url gambar
            //     description:"", //deskripsi
            // });
            // setIsChecked(current => !current)
            alert("Product berhasil dirubah")
            navigate("/table");
        } catch (error) {
            if(error.response){
                // console.log(error)
                // console.log(`server responded with status code: `, error.response.status)
                alert(error.response.data.info)
            } else if (error.request) {
                alert('No response received:', error.request);
            } else {
                alert('Error creating request:', error.message);
            }
        }
    };
    const {handleChange, values, handleSubmit, errors, touched, handleBlur, setFieldTouched, setFieldValue} = useFormik({
        initialValues: input,
        enableReinitialize: true,
        onSubmit: onSubmit,
        validationSchema: validationSchema,
    });

    

  // melakukan fetch product detail
  const fetchproductDetail = async () => {
    try {
      // fetch data menggunakan axios
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${productId}`
      );
      console.log(response.data.data)
      const product = response.data.data;
      // kita memasukkan data dari server ke dalam form nya
      setInput({
        name: product.name,
        stock: product.stock,
        harga: product.harga,
        is_diskon:product.is_diskon,
        harga_diskon:product.harga_diskon,
        category:product.category,
        image_url: product.image_url,
        description:product.description,
      });
    } catch (error) {
      alert(`Terjadi Sesuatu Error : ${error}`);
      console.log(error)
    }
  };

  useEffect(() => {
    fetchproductDetail();
  }, []);
  console.log(input)
  

  return (
    <div className="m-5">
        <Helmet>
            <title>Update Form id : {productId} Page</title>
        </Helmet>
            <div>
              <span className="self-center text-xl text-orange-400 font-semibold">Update Products</span>
            </div>
            <div className="grid grid-cols-5 gap-2 max-md:grid-cols-1 max-md:gap-0 pt-5">
                <div className="col-span-3 mb-6 max-md:col-span-1">
                    <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900">Nama Barang</label>
                    <input 
                    onChange={handleChange}
                    type="text" 
                    name="name" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                    placeholder="Masukan Nama Barang" 
                    value={values.name}
                    onBlur={handleBlur}
                    />
                    { touched.name === true && errors.name != null && (
                        <p className="text-red-500 text-sm">{errors.name}</p>
                    )}
                </div>
                <div className="col-span-2 mb-6 max-md:col-span-1">
                    <label htmlFor="stock" className="block mb-2 text-sm font-medium text-gray-900">Stock Barang</label>
                    <input
                    onChange={handleChange}
                    type="number" 
                    name="stock" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
                    placeholder="Masukan Jumlah Stock Barang"
                    value={values.stock}
                    onBlur={handleBlur}/>
                    { touched.name === true && errors.stock != null && (
                        <p className="text-red-500 text-sm">{errors.stock}</p>
                    )}
                </div>
                <div className="col-span-2 mb-6 max-md:col-span-1">
                    <label htmlFor="harga" className="block mb-2 text-sm font-medium text-gray-900">Harga Barang</label>
                    <input
                    onChange={handleChange} 
                    type="number" 
                    name="harga" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                    placeholder="Masukan Harga Barang" 
                    value={values.harga}
                    onBlur={handleBlur}/>
                    { touched.name === true && errors.harga != null && (
                        <p className="text-red-500 text-sm">{errors.harga}</p>
                    )}
                </div>
                <div className="flex items-end justify-center py-3 mb-6 max-md:col-span-1 max-md:justify-start">
                    <div className="flex items-center h-5">
                    <input 
                        onChange={(event) => setFieldValue("is_diskon", event.target.checked)} 
                        onBlur={() => setFieldTouched("is_diskon")}
                        name="is_diskon" 
                        type="checkbox" 
                        id="single"
                        checked={values.is_diskon}
                        className="w-4 h-4 border border-gray-300 rounded bg-gray-50"
                         />
                    </div>
                    <label htmlFor="is_diskon" className="ml-2 text-sm font-medium text-gray-900">Status Diskon</label>
                </div>
                <div className=" col-span-2 mb-6 max-md:col-span-1">
                    {values.is_diskon === true &&
                    (<div className=""> 
                        <label htmlFor="harga_diskon" className="block mb-2 text-sm font-medium text-gray-900">Harga Diskon</label>
                        <input 
                        onChange={handleChange}
                        type="number" 
                        name="harga_diskon" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
                        placeholder="Masukan Harga Diskon Barang" 
                        value={values.harga_diskon}
                        
                    />
                    { touched.name === true && values.is_diskon === true && errors.harga_diskon != null && (
                        <p className="text-red-500 text-sm">{errors.harga_diskon}</p>
                    )}
                    </div>)
                    }
                </div>
                <div className="col-span-2 mb-6 max-md:col-span-1">
                    <label htmlFor="category" className="block mb-2 text-sm font-medium text-gray-900">Kategori Barang</label>
                    <select onChange={(event) => setFieldValue("category", event.target.value)} 
                        onBlur={() => setFieldTouched("category")}
                         className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" value={input.category}  >
                        <option value="" disabled>Pilih kategori</option>
                        <option value="teknologi">Teknologi</option>
                        <option value="makanan">Makanan</option>
                        <option value="minuman">Minuman</option>
                        <option value="hiburan">Hiburan</option>
                        <option value="kendaraan">Kendaraan</option>
                    </select>
                </div>
                <div className="col-span-3 mb-6 max-md:col-span-1">
                    <label htmlFor="image_url" className="block mb-2 text-sm font-medium text-gray-900">Gambar Barang</label>
                    <input
                    onChange={handleChange} 
                    type="text" 
                    name="image_url" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                    placeholder="Masukan Harga Barang" 
                    value={values.image_url}
                    onBlur={handleBlur}/>
                    { touched.name === true && errors.image_url != null && (
                        <p className="text-red-500 text-sm">{errors.image_url}</p>
                    )}
                </div>
                <div className="col-span-5 mb-6 max-md:col-span-1">              
                    <label htmlFor="description" className="block mb-2 text-sm font-medium text-gray-900">Deskripsi</label>
                    <textarea onChange={handleChange} name="description" rows="5" className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Masukan deskripsi" value={values.description}></textarea>
                </div>
            </div>
            <div className="flex justify-end">
                <button 
                // onClick={navigate("/table")}
                type="submit" 
                className="text-orange-400  bg-white border-2 border-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                Cancel</button>
                <button 
                    onClick={handleSubmit}
                    type="submit" 
                    className="text-white bg-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                    Submit</button>
            </div>
        </div>
  );
}

export default UpdateForm;
