import React, {useContext,useState} from "react";
import { GlobalContext } from "../context/GlobalContext";

function Form({input}){
    const { handleChange , handleSubmit, setInput, isChecked } = useContext(GlobalContext);
    // const [ setIsChecked] = useState(false);
    const pathname = window.location.pathname
    console.log(input)

    return (
        <div className="m-5">
            <div>
                {pathname === '/create' ? 
                    <span className="self-center text-xl text-orange-400 font-semibold">Create Products</span> :
                    <span className="self-center text-xl text-orange-400 font-semibold">Update Products</span>
                }
                
            </div>
            <div className="grid grid-cols-5 gap-2 max-md:grid-cols-1 max-md:gap-0 pt-5">
                <div className="col-span-3 mb-6 max-md:col-span-1">
                    <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900">Nama Barang</label>
                    <input 
                    onChange={handleChange}
                    type="text" 
                    name="name" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                    placeholder="Masukan Nama Barang" 
                    value={input.name}
                    required/>
                </div>
                <div className="col-span-2 mb-6 max-md:col-span-1">
                    <label htmlFor="stock" className="block mb-2 text-sm font-medium text-gray-900">Stock Barang</label>
                    <input
                    onChange={handleChange}
                    type="number" 
                    name="stock" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
                    placeholder="Masukan Jumlah Stock Barang"
                    value={input.stock}
                    required/>
                </div>
                <div className="col-span-2 mb-6 max-md:col-span-1">
                    <label htmlFor="harga" className="block mb-2 text-sm font-medium text-gray-900">Harga Barang</label>
                    <input
                    onChange={handleChange} 
                    type="number" 
                    name="harga" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                    placeholder="Masukan Harga Barang" 
                    value={input.harga}
                    required/>
                </div>
                <div className="flex items-end justify-center py-3 mb-6 max-md:col-span-1 max-md:justify-start">
                    <div className="flex items-center h-5">
                    <input 
                        onChange={handleChange}
                        name="is_diskon" 
                        type="checkbox" 
                        value={isChecked} 
                        id="single"
                        checked={input.is_diskon}
                        className="w-4 h-4 border border-gray-300 rounded bg-gray-50"
                         />
                    </div>
                    <label htmlFor="is_diskon" className="ml-2 text-sm font-medium text-gray-900">Status Diskon</label>
                </div>
                <div className=" col-span-2 mb-6 max-md:col-span-1">
                    {isChecked &&
                    <div className=""> 
                        <label htmlFor="harga_diskon" className="block mb-2 text-sm font-medium text-gray-900">Harga Diskon</label>
                        <input 
                        onChange={handleChange}
                        type="number" 
                        name="harga_diskon" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" 
                        placeholder="Masukan Harga Diskon Barang" 
                        value={input.harga_diskon}
                    />
                    </div>
                    }
                </div>
                <div className="col-span-2 mb-6 max-md:col-span-1">
                    <label htmlFor="category" className="block mb-2 text-sm font-medium text-gray-900">Kategori Barang</label>
                    <select onChange={handleChange} name="category" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" value={input.category}  >
                        <option value="" disabled>Pilih kategori</option>
                        <option value="teknologi">Teknologi</option>
                        <option value="makanan">Makanan</option>
                        <option value="minuman">Minuman</option>
                        <option value="hiburan">Hiburan</option>
                        <option value="kendaraan">Kendaraan</option>
                    </select>
                </div>
                <div className="col-span-3 mb-6 max-md:col-span-1">
                    <label htmlFor="image_url" className="block mb-2 text-sm font-medium text-gray-900">Gambar Barang</label>
                    <input
                    onChange={handleChange} 
                    type="text" 
                    name="image_url" 
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5" 
                    placeholder="Masukan Harga Barang" 
                    value={input.image_url}
                    required/>
                </div>
                <div className="col-span-5 mb-6 max-md:col-span-1">              
                    <label htmlFor="description" className="block mb-2 text-sm font-medium text-gray-900">Deskripsi</label>
                    <textarea onChange={handleChange} name="description" rows="5" className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Masukan deskripsi" value={input.description}></textarea>
                </div>
            </div>
            <div className="flex justify-end">
                <button 
                onClick={handleSubmit}
                type="submit" 
                className="text-orange-400  bg-white border-2 border-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                Cancel</button>
                {pathname === '/create' ? 
                    <button 
                    onClick={handleSubmit}
                    type="submit" 
                    className="text-white bg-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                    Submit</button> :
                    <button 
                    onClick={handleSubmit}
                    type="submit" 
                    className="text-white bg-orange-400 hover:bg-orange-500 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 mx-2 text-center">
                    Update</button>
                }
                
            </div>
        </div>
    )
}

export default Form;