import React, { useContext, useEffect } from "react";
import Navbar from "../components/NavBar";
import Table from "../components/Table"
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../layout/Layout";

function TablePage() {

   const { fetchProducts } = useContext(GlobalContext);

   useEffect(() => {
     fetchProducts();
   }, []);


  return (
    <div >
      <Layout>
        <Table/>
      </Layout>
    </div>
  );
}

export default TablePage;
