import React from "react";
import CreateForm from "../components/CreateForm";
import Navbar from "../components/NavBar";
import Layout from "../layout/Layout";

function CreatePage() {
  return (
    <div>
      <Layout>
        <CreateForm />
      </Layout>
    </div>
  );
}

export default CreatePage;
