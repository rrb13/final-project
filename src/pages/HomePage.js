import React, { useContext, useEffect } from "react";
import Product from "../components/Product";
import Navbar from "../components/NavBar";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../layout/Layout";
import {Helmet} from "react-helmet"

function HomePage() {
  const { products, fetchProducts, loading } = useContext(GlobalContext);

    useEffect(() => {
      fetchProducts();
    }, []);


  return (
    <div>
      {/* NAVBAR */}
      <Layout>
        <Helmet>
            <title>Home Page</title>
        </Helmet>
        <div className="max-w-7xl mx-auto my-10">
        {/* menu */}
        <h1 className="self-center text-2xl text-orange-400 font-semibold  my-5">Catalog Products</h1>
        {/* list of card products */}
        <div className="grid grid-cols-4 max-md:grid-cols-2 gap-4 pt-2">
          {products.map((product) => (
            <Product key={product.id} product={product} />
          ))}
        </div>
      </div>
      </Layout>
      
    </div>
  );
}

export default HomePage;
