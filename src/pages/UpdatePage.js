import React from "react";
import Navbar from "../components/NavBar";
import UpdateForm from "../components/UpdateForm";
import Layout from "../layout/Layout";

function UpdatePage() {
  return (
    <div>
      <Layout>
        <UpdateForm/>
      </Layout>
    </div>
  );
}

export default UpdatePage;
